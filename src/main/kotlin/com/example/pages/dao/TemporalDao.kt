package com.example.pages.dao

import com.example.pages.model.Movie
import com.example.pages.model.MovieData
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.*

class TemporalDao {
    private val classPathString = TemporalDao::class.java.`package`.name.replace('.', '/')
    private val packagePath: Path = Paths.get("src", "main", "kotlin", classPathString, "dates")

    private val moviesFilePath : Path = Path.of(packagePath.toString(), "movies.json")
    val imagesFilePath : Path = Path.of(packagePath.toString(), "images")

    init {
        if (packagePath.notExists()) {
            packagePath.createDirectory()
        }
        if (imagesFilePath.notExists()) {
            imagesFilePath.createDirectory()
        }
        if (moviesFilePath.notExists()) {
            moviesFilePath.createFile()
            moviesFilePath.writeText(Json.encodeToString(listOf<Movie>()))
        }
    }

    private val readMoviesFile: List<Movie> get() =
        Json.decodeFromString(moviesFilePath.readText())

    private val moviesListIsEmpty get() = readMoviesFile.isEmpty()

    private fun newId() = readMoviesFile.maxOf { it.id } + 1

    private fun rewriteMovieJson(list: List<Movie>) =
        moviesFilePath.writeText(list.encodeToJson())

    private fun List<Movie>.encodeToJson() = Json.encodeToString(this)

    fun getAll(): List<Movie> = readMoviesFile

    fun getFromId(id: Int) = readMoviesFile.find { it.id == id }
    fun getNextFileName(): String {
        if (moviesListIsEmpty) {
            return "image0.jpg"
        }
        return "image${newId()}.jpg"
    }

    private fun taskCreation(movieData: MovieData): Movie =
        Movie(
            newId(),
            movieData.title,
            movieData.year,
            movieData.genre,
            movieData.director,
            movieData.imageName
        )

    fun create(movieData: MovieData) {
        if (moviesListIsEmpty){
            val movie = Movie(0,
                movieData.title,
                movieData.year,
                movieData.genre,
                movieData.director,
                movieData.imageName)
            moviesFilePath.writeText(Json.encodeToString(listOf(movie)))
        } else {
            val movie = taskCreation(movieData)
            val movies = readMoviesFile.toMutableList()
            movies.add(movie)
            rewriteMovieJson(movies)
        }
    }
}