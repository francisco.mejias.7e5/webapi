package com.example.pages.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Movie(
    val id: Int,
    val title: String,
    val year: String,
    val genre: String,
    val director: String,
    @SerialName("image_name") val imageName: String
)