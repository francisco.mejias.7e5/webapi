package com.example.pages.model

class MovieData(
    val title: String,
    val year: String,
    val genre: String,
    val director: String,
    val imageName: String
)
