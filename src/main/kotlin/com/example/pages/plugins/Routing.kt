package com.example.pages.plugins

import com.example.pages.dao.TemporalDao
import com.example.pages.routes.pagesRouting
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        pagesRouting()
        static("/static") {
            resources("files")
            static("/images") {
                resources("files/images")
            }
            static("/css") {
                resources("files/css")
            }
            static("/films") {
                files("${TemporalDao().imagesFilePath}")
            }
        }
    }
}
