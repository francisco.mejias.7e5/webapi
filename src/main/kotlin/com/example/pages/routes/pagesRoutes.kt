package com.example.pages.routes

import com.example.pages.dao.TemporalDao
import com.example.pages.model.MovieData
import com.example.pages.templates.MainHtmlTemplate
import com.example.pages.templates.MainHtmlTemplate.PageType
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.nio.file.Path
import kotlin.io.path.outputStream
import kotlin.properties.Delegates

fun Route.pagesRouting() {
    val temporalDao = TemporalDao()
    get("/") {
        call.respondRedirect("films/all")
    }
    get("/films") {
        call.respondRedirect("films/all")
    }
    route("/films") {
        get("all") {
            call.respondHtmlTemplate(MainHtmlTemplate()) {
                this.pageType = PageType.ALL
                this.movies = temporalDao.getAll()
            }
        }
        get("new") {
            call.respondHtmlTemplate(MainHtmlTemplate()) {
                this.pageType = PageType.NEW
            }
        }
        post {
            val multipart = call.receiveMultipart().readAllParts()
            var title: String by Delegates.notNull()
            var year: String by Delegates.notNull()
            var genre: String by Delegates.notNull()
            var director: String by Delegates.notNull()
            var imageName: String by Delegates.notNull()

            if (multipart.map { it.name } == listOf("title", "year", "genre", "director", "image")) {
                multipart.forEach { part ->
                    if (part is PartData.FormItem) {
                        when (part.name) {
                            "title" -> title = part.value
                            "year" -> year = part.value
                            "genre" -> genre = part.value
                            "director" -> director = part.value
                        }
                    }

                    // Copiado de esta página
                    // https://ryanharrison.co.uk/2018/09/20/ktor-file-upload-download.html

                    // if part is a file (could be form item)
                    if (part is PartData.FileItem) {
                        // retrieve file name of upload

                        val name = temporalDao.getNextFileName()
                        val file = Path.of("${temporalDao.imagesFilePath}/$name")

                        imageName = name

                        // use InputStream from part to save file
                        part.streamProvider().use { its ->
                            // copy the stream to the file with buffering
                            file.outputStream().buffered().use {
                                // note that this is blocking
                                its.copyTo(it)
                            }
                        }
                    }
                    part.dispose()
                }
                try {
                    temporalDao.create(MovieData(title, year, genre, director, imageName))
                } catch (e: IllegalStateException) {
                    return@post call.respondText(
                        "Bad Parameters",
                        status = HttpStatusCode.BadRequest
                    )
                }
            }
        }
        get("detail/{id?}") {
            val id = call.parameters["id"]?.toIntOrNull() ?: return@get call.respondText(
                "Bad Parameters",
                status = HttpStatusCode.BadRequest
            )
            val movie = temporalDao.getFromId(id) ?: return@get call.respondText(
                "Not Found",
                status = HttpStatusCode.NotFound
            )
            call.respondHtmlTemplate(MainHtmlTemplate()) {
                this.pageType = PageType.DETAIL
                this.movies = listOf(movie)
            }
        }
        get("about") {
            call.respondHtmlTemplate(MainHtmlTemplate()) {
                this.pageType = PageType.ABOUT
            }
        }
    }
}