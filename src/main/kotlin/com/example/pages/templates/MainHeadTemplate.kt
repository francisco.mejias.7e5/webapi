package com.example.pages.templates

import io.ktor.server.html.*
import kotlinx.html.HEAD
import kotlinx.html.link
import kotlinx.html.meta
import kotlinx.html.title

class MainHeadTemplate : Template<HEAD> {
    override fun HEAD.apply() {
        title {
            +"Movie Register"
        }
        meta {
            charset = "UTF-8"
        }
        link {
            rel = "icon"
            type = "image/png"
            sizes = "32x32"
            href = "/static/images/logo.png"
        }
        link {
            rel = "stylesheet"
            href = "/static/css/main.css"
        }
    }
}
