package com.example.pages.templates

import com.example.pages.model.Movie
import com.example.pages.templates.MainHtmlTemplate.PageType.*
import com.example.pages.templates.all.BodyAll
import com.example.pages.templates.detail.BodyDetail
import com.example.pages.templates.form.BodyForm
import io.ktor.server.html.*
import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.head

open class MainHtmlTemplate: Template<HTML> {
    lateinit var pageType: PageType
    var movies: List<Movie> = listOf()
    override fun HTML.apply() {
        head {
            insert(MainHeadTemplate(), TemplatePlaceholder())
        }
        body {
            when(pageType) {
                ALL -> insert(BodyAll(movies), TemplatePlaceholder())
                NEW -> insert(BodyForm(), TemplatePlaceholder())
                DETAIL -> insert(BodyDetail(movies.first()), TemplatePlaceholder())
                ABOUT -> TODO()
            }
        }
    }

    enum class PageType {
        ALL,
        NEW,
        DETAIL,
        ABOUT
    }
}