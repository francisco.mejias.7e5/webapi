package com.example.pages.templates.all

import com.example.pages.model.Movie
import io.ktor.server.html.*
import kotlinx.html.*

class BodyAll(private val movies: List<Movie>) : Template<BODY> {
    override fun BODY.apply() {
        header {
            figure {
                img {
                    src = "/static/images/logo.png"
                    width = "300"
                    height = "300"
                    alt = "Movies Logo"
                }
            }
        }
        nav {
            ul {
                li {
                    a {
                        href = "http://0.0.0.0:8080/films/all"
                        +"Listado de peliculas"
                    }
                }
                li {
                    a {
                        href = "http://0.0.0.0:8080/films/new"
                        +"Registrar pelicula"
                    }
                }
            }
        }
        main {
            h2 { +"Llista de pelis" }
            table {
                movies.forEach {
                    tr {
                        th { +"Portada pelicula" }
                        th { +"Titulo" }
                        th { +"Mas detalles" }
                    }
                    tr {
                        td {
                            img {
                                src = "/static/films/${it.imageName}"
                                width = "200"
                                height = "200"
                                alt = "Movie Image"
                            }
                        }
                        td { +it.title }
                        td {
                            a {
                                href = "http://0.0.0.0:8080/films/detail/?id=${it.id}"
                                +"More details"
                            }
                        }
                    }
                }
            }
        }
    }
}