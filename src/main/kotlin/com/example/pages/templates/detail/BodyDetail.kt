package com.example.pages.templates.detail

import com.example.pages.model.Movie
import io.ktor.server.html.*
import kotlinx.html.*

class BodyDetail(private val movie: Movie): Template<BODY> {
    override fun BODY.apply() {
        main {
            h2 { +movie.title }
            figure {
                img {
                    src = "/static/films/${movie.imageName}"
                    width = "400"
                    height = "400"
                    alt = "Movie Image"
                }
            }
            p { +"Año: ${movie.year}" }
            p { +"Genero: ${movie.genre}" }
            p { +"Director: ${movie.director}" }
        }
    }
}