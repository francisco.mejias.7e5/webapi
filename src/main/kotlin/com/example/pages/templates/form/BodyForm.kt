package com.example.pages.templates.form

import io.ktor.server.html.*
import kotlinx.html.*

class BodyForm: Template<BODY> {
    override fun BODY.apply() {
        main {
            h2 { +"Registrar Pelicula" }
            form {
                action = "http://localhost:8080/films"
                method = FormMethod.post
                encType = FormEncType.multipartFormData
                label {
                    htmlFor = "title"
                    +"Titulo:"
                }
                br {
                }
                input {
                    type = InputType.text
                    id = "title"
                    name = "title"
                }
                br {
                }
                br {
                }
                label {
                    htmlFor = "year"
                    +"Año:"
                }
                br {
                }
                input {
                    type = InputType.text
                    id = "year"
                    name = "year"
                }
                br {
                }
                br {
                }
                label {
                    htmlFor = "genre"
                    +"Genero:"
                }
                br {
                }
                input {
                    type = InputType.text
                    id = "genre"
                    name = "genre"
                }
                br {
                }
                br {
                }
                label {
                    htmlFor = "director"
                    +"Director:"
                }
                br {
                }
                input {
                    type = InputType.text
                    id = "director"
                    name = "director"
                }
                br {
                }
                br {
                }
                label {
                    htmlFor = "image"
                    +"Imagen:"
                }
                br {
                }
                input {
                    type = InputType.file
                    id = "image"
                    name = "image"
                }
                br {
                }
                br {
                }
                input {
                    type = InputType.submit
                    value = "Registrar"
                }
            }
        }
    }
}